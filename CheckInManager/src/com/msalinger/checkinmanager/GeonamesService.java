package com.msalinger.checkinmanager;

import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

/** 
 * Class for connecting to Geonames Service.  Extends RESTService and executes 
 * call to get POI information.  
 * 
 * @author Michael Salinger
 * 
 */
public class GeonamesService extends RESTService {
	private final static String TAG = "CHECKINSERVICE";
	/**
	 * Constructor. Defines the service and adds Lat, Lon, and Username to the service.  
	 * 
	 * @param url	URL for Geonames Service
	 * @param lat	Latitude 
	 * @param lon	Longitude
	 * @param username	Geonames Username
	 */
	public GeonamesService(String url, double lat, double lon, String username) {
		super(url);
		this.addParam("lat", Double.toString(lat));
		this.addParam("lng", Double.toString(lon));
		this.addParam("username", username);
	}
	
	/**
	 * Retrieves POI data from Geonames Web Service
	 * 
	 * @return	JSONObject of POI data
	 * @throws JSONException 
	 */
	public JSONObject getPOIs() throws JSONException {
		try {
			this.execute(RequestMethod.GET);
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return null;
		}
		return this.getJSONResponse();
	}

}