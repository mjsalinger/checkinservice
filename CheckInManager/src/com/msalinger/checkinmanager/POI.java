/**
 * 
 */
package com.msalinger.checkinmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.location.Geocoder;
import android.location.Address;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.content.Context;


/**
 * This object encapsulates Point of Interest data for a particular set of 
 * lat/lon coordinates.  Implements Parceable.
 * 
 * @author Michael Salinger
 *
 */
public class POI implements Parcelable {
	// Private Members
	
	private final static String TAG = "CHECKINSERVICE";
	
	private String poiName;
	private double latitude;
	private double longitude;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String poiType;
	private ArrayList<String> checkedInUsers = new ArrayList<String>();
	private Context currentContext;

	// Constructors
	
	/**
	 * Sets up a POI object.  If address info is unavailable, automatically 
	 * fetches address info from Android Geoloc methods.  
	 * 
	 * @param lat	Latitude
	 * @param lng	Longitude
	 * @param cont	Context for Geoloc
	 */
	public POI(double lat, double lng, Context cont) {
		this.latitude = lat;
		this.longitude = lng;
		this.currentContext = cont;
		if (this.address == null) {
			fetchAddressInfo();
		}
	}
	
	/**
	 * Sets up a POI object.  If address info is unavailable, automatically 
	 * fetches address info from Android Geoloc methods.
	 * 
	 * @param lat	Latitude
	 * @param lng	Longitude
	 * @param cont	Context for Geoloc
	 * @param name	Name of the POI
	 * @param addr	Address of POI
	 * @param locCity	City of POI
	 * @param st	State of POI
	 * @param zipcode	Zip Code of POI
	 * @param type	POI Type (e.g. Restaurant, park, etc.)
	 * @param users	Users checked into POI 
	 */
	public POI(double lat, double lng, Context cont, String name, String addr, String locCity, String st, String zipcode, String type, ArrayList<String> users) {
		this.latitude = lat;
		this.longitude = lng;
		this.poiName = name;
		this.address = addr;
		this.city = locCity;
		this.state = st;
		this.zip = zipcode;
		this.poiType=type;
		
		if (users != null) {
			this.checkedInUsers = users;
		}
		
		this.currentContext = cont;
		
		if (this.address == null) {
			fetchAddressInfo();
		}
	}
	
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public POI(Parcel in) {
		readFromParcel(in);
	}
	
	/**
	 * Sets up a POI object.  If address info is unavailable, automatically 
	 * fetches address info from Android Geoloc methods.
	 * 
	 * @param lat	Latitude
	 * @param lng	Longitude
	 * @param cont	Context for Geoloc
	 * @param name	Name of the POI
	 * @param type	POI Type (e.g. Restaurant, park, etc.)
	 */
	public POI(double lat, double lng, Context cont, String name, String type) {
		this(lat, lng, cont, name, null, null, null, null, type, null);
	}

	public String getPoiName() {
		return poiName;
	}

	public void setPoiName(String poiName) {
		this.poiName = poiName;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPoiType() {
		return poiType;
	}

	public void setPoiType(String poiType) {
		this.poiType = poiType;
	}

	public ArrayList<String> getCheckedInUsers() {
		return checkedInUsers;
	}

	public void setCheckedInUsers(ArrayList<String> checkedInUsers) {
		this.checkedInUsers = checkedInUsers;
	}

	@Override
	public String toString() {
		return poiName;
	}
	
	/**
	 * Adds user to POI checkin list.  Note this adds the user in the object
	 * only.  It does not persist across sessions unless saved elsewhere.    
	 * 
	 * @param userName	User to check in
	 * @return			True/false for success/failure
	 */
	public boolean checkIn(String userName) {
		checkedInUsers.add(userName);
		return true;
	}
	
	/**
	 * Uses Android Geoloc to auto-update the POI with address info
	 */
	private void fetchAddressInfo() {
		Geocoder gc = new Geocoder(currentContext, Locale.getDefault());
		
		try {
			List<Address> addresses = gc.getFromLocation(latitude, longitude, 1);
			if (addresses.size() > 0) { 
				Address address = addresses.get(0);
				this.address = address.getPremises();
				this.address = address.getAddressLine(0);
				this.city = address.getLocality();
				this.state = address.getAdminArea();
				this.zip = address.getPostalCode();				
			}  
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
		} 
	}

	
	// Parceable Methods
	public int describeContents() {
		return 0;
	}
	
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(poiName);
		dest.writeDouble(latitude);
		dest.writeDouble(longitude);
		dest.writeString(address);
		dest.writeString(city);
		dest.writeString(state);
		dest.writeString(zip);
		dest.writeString(poiType);
		dest.writeStringList(checkedInUsers);
	}
	
	private void readFromParcel(Parcel in) {
		 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		this.poiName = in.readString();
		this.latitude = in.readDouble();
		this.longitude = in.readDouble();
		this.address = in.readString();
		this.city = in.readString();
		this.state = in.readString();
		this.zip = in.readString();
		this.poiType = in.readString();
		in.readStringList(checkedInUsers);
	}
	
	/**
    *
    * This field is needed for Android to be able to
    * create new objects, individually or as arrays.
    *
    * This also means that you can use use the default
    * constructor to create the object and use another
    * method to hyrdate it as necessary.
    *
    * I just find it easier to use the constructor.
    * It makes sense for the way my brain thinks ;-)
    *
    */
   public static final Parcelable.Creator CREATOR =
   	new Parcelable.Creator() {
           public POI createFromParcel(Parcel in) {
               return new POI(in);
           }

           public POI[] newArray(int size) {
               return new POI[size];
           }
       };
}
